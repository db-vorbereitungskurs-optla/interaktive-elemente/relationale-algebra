const $ = require('jquery');

// R1 und S1 ----------------------------------------
const R1 = [];
const S1 = [];
R1[0] = ['A', 'B', 'C'];
R1[1] = ['a', 'b', 'c'];
R1[2] = ['d', 'a', 'f'];
R1[3] = ['c', 'b', 'd'];

S1[0] = ['A', 'B', 'C']; // D, E, F
S1[1] = ['b', 'g', 'a'];
S1[2] = ['d', 'a', 'f'];

const roderS1Title = 'R &cup; S';
const RoderS1 = [];
RoderS1[0] = ['A', 'B', 'C'];
RoderS1[1] = ['a', 'b', 'c'];
RoderS1[2] = ['d', 'a', 'f'];
RoderS1[3] = ['c', 'b', 'd'];
RoderS1[4] = ['b', 'g', 'a'];

const RminusS1Title = 'R - S';
const RminusS1 = [];
RminusS1[0] = ['A', 'B', 'C'];
RminusS1[1] = ['a', 'b', 'c'];
RminusS1[2] = ['c', 'b', 'd'];

/* let RXS1Title = 'R x S';
let RXS1 = [];
RXS1[0] = [ 'A', 'B', 'C', 'D', 'E', 'F'];
RXS1[1] = [ 'a', 'b', 'c', 'b', 'g', 'a'];
RXS1[2] = [ 'd', 'a', 'f', 'b', 'g', 'a'];
RXS1[3] = [ 'c', 'b', 'd', 'b', 'g', 'a'];
RXS1[4] = [ 'a', 'b', 'c', 'd', 'a', 'f'];
RXS1[5] = [ 'd', 'a', 'f', 'd', 'a', 'f'];
RXS1[6] = [ 'c', 'b', 'd', 'd', 'a', 'f']; */

const RpiS1Titel = '&#960; AB (R)';
const RpiS1 = [];
RpiS1[0] = ['A', 'B'];
RpiS1[1] = ['a', 'b'];
RpiS1[2] = ['d', 'a'];
RpiS1[3] = ['c', 'b'];

// R2 und S2 ----------------------------------------
const R2 = [];
R2[0] = ['A', 'B', 'C', 'D'];
R2[1] = ['1', '2', '3', '4'];
R2[2] = ['4', '5', '6', '7'];
R2[3] = ['7', '8', '9', '0'];
const S2 = [];
S2[0] = ['E', 'F', 'G'];
S2[1] = ['1', '2', '3'];
S2[2] = ['7', '8', '9'];

const RXS2Title = 'R x S';
const RXS2 = [];
RXS2[0] = ['A', 'B', 'C', 'D', 'E', 'F', 'G'];
RXS2[1] = ['1', '2', '3', '4', '1', '2', '3'];
RXS2[2] = ['4', '5', '6', '7', '1', '2', '3'];
RXS2[3] = ['7', '8', '9', '0', '1', '2', '3'];
RXS2[4] = ['1', '2', '3', '4', '7', '8', '9'];
RXS2[5] = ['4', '5', '6', '7', '7', '8', '9'];
RXS2[6] = ['7', '8', '9', '0', '7', '8', '9'];

const RsigmaS2Title = '&#963; C=a (R)'; // ?
const RsigmaS2 = [];
RsigmaS2[0] = ['A', 'B', 'C', 'D'];
RsigmaS2[1] = ['1', '2', 'a', '4'];
RsigmaS2[2] = ['4', '5', 'a', '7'];
RsigmaS2[3] = ['7', '8', 'a', '0'];

// R3 und S3 ---------------------------------------------
const R3 = [];
R3[0] = ['A', 'B', 'C', 'D'];
R3[1] = ['b', '2', 'd', '5'];
R3[2] = ['4', 'a', 'a', '7'];
R3[3] = ['a', 'd', '9', 'c'];
const S3 = [];
S3[0] = ['A', 'C', 'G'];
S3[1] = ['b', 'd', '3'];
S3[2] = ['5', '2', '9'];
S3[3] = ['b', 'd', 'a'];
S3[4] = ['a', '9', 'a'];

const RjoinS3Title = 'R &#10781; S';
const RjoinS3 = [];
RjoinS3[0] = ['A', 'B', 'C', 'D', 'G'];
RjoinS3[1] = ['b', '2', 'd', '5', '3'];
RjoinS3[2] = ['b', '2', 'd', '5', 'a'];
RjoinS3[3] = ['a', 'd', '9', 'c', 'a'];

let RActuell;
let SActuell;
let RSActuell;
let RSTitle;
let accidentNr;

// ---------------------------------------------
$(document).ready(function() {
	getNewRelation();

	$('#newTaskButton').click(function() {
		$('#positivResult').empty();
		$('#negativResult').empty();
		getNewRelation();
	});

	$('#solutionButton').click(function() {
		$('#positivResult').empty();
		$('#negativResult').empty();
		fillInSolution(RSActuell);
	});

	$('#checkButton').click(function() {
		$('#positivResult').empty();
		$('#negativResult').empty();
		checkTable(RSActuell);
	});

	$('#resetButton').click(function() {
		$('#positivResult').empty();
		$('#negativResult').empty();
		resetTable(RSActuell);
	});

	$('.btn-game-info').on('click', function() {
		// btn to show Message Box and Message Box elements
		const message_box = document.getElementById('box-game-infos');
		message_box.style.display='block';
		// set the close-button
		for (let i = 0; i != message_box.children.length; i+=1) {
			if (message_box.children[i].id === 'btn_close_message') {
				message_box.children[i].addEventListener('click', function() {
					message_box.style.display = "none";
				});
			}
		}
	})
});

function getNewRelation() {
	let newAccidentNr = accident(1, 7);
	while (accidentNr === newAccidentNr) {
		newAccidentNr = accident(1, 7);
	}
	accidentNr = newAccidentNr;
	switch (accidentNr) {
		case 1:
			RActuell = R1;
			SActuell = S1;
			RSActuell = RoderS1;
			RSTitle = roderS1Title;
		break;
		case 2:
			RActuell = R1;
			SActuell = S1;
			RSActuell = RpiS1;
			RSTitle = RpiS1Titel;
		break;
		case 3:
			RActuell = R1;
			SActuell = S1;
			RSActuell = RminusS1;
			RSTitle = RminusS1Title;
		break;
		case 4:
			RActuell = R2;
			SActuell = S2;
			RSActuell = RXS2;
			RSTitle = RXS2Title;
		break;
		case 5:
			RActuell = R2;
			SActuell = S2;
			RSActuell = RsigmaS2;
			RSTitle = RsigmaS2Title;
		break;
		case 6:
			RActuell = R3;
			SActuell = S3;
			RSActuell = RjoinS3;
			RSTitle = RjoinS3Title;
		break;
	}
	settables(RActuell, SActuell, RSActuell, RSTitle);
}

function settables(Rn, Sn, RSn, title) {
	$('#table-left').empty();
	$('#table-right').empty();
	$('#title').empty();
	$('#content').empty();

	// RTable ////////////
	let RTable = '<table id="relAlgebraR"><caption>R</caption><thead><tr>';
	for (let i = 0; i < Rn[0].length; i++) {
		RTable += '<td>' + Rn[0][i] + '</td>';
	}
	RTable += '</tr></thead><tbody>';

	for (let i = 1; i < Rn.length; i++) {
		RTable += '<tr>';
		for (let ii = 0; ii < Rn[i].length; ii++) {
			RTable += '<td>' + Rn[i][ii] + '</td>';
		}
		RTable += '</tr>';
	}
	RTable += '</tbody></table>';
	$('#table-left').append(RTable);

	// STable ////////////
	let STable = '<table id="relAlgebraS"><caption>S</caption><thead><tr>';
	for (let i = 0; i < Sn[0].length; i++) {
		STable += '<td>' + Sn[0][i] + '</td>';
	}
	STable += '</tr></thead><tbody>';

	for (let i = 1; i < Sn.length; i++) {
		STable += '<tr>';
		for (let ii = 0; ii < Sn[i].length; ii++) {
			STable += '<td>' + Sn[i][ii] + '</td>';
		}
		STable += '</tr>';
	}
	STable += '</tbody></table>';
	$('#table-right').append(STable);

	// RSTable empty /////
	let RSTable = '<table id="relAlgebra"><caption id="titleX">test</caption><thead><tr>';
	for (let h = 0; h < RSn[0].length; h++) {
		RSTable += '<td><input class="txtInput" type="text" aria-label="Eingabe Tabellenkopf" id="RS0' + h + '" /></td>';
	}
	RSTable += '</tr></thead><tbody>';

	for (let i = 1; i < RSn.length; i++) {
		RSTable += '<tr>';
		for (let ii = 0; ii < RSn[i].length; ii++) {
			RSTable += '<td><input class="txtInput" type="text" aria-label="Eingabe" id="RS' + i + ii + '" /></td>';
		}
		RSTable += '</tr>';
	}
	RSTable += '</tbody></table>';

	$('#content').append(RSTable);
	$('#titleX').html(title);
}

function checkTable(correctRS) {
	let userRS = [];
	// 1. get all Values from userinput
	for (let n = 0; n < correctRS.length; n++) {
		userRS[n] = [];
		for (let nn = 0; nn < correctRS[n].length + 1; nn++) {
			if (nn === correctRS[n].length) {
				userRS[n][nn] = '';
			} else {
				userRS[n][nn] = $('input:text[id=RS' + n + nn + ']').val();
			}
		}
	}

	// 2. compare to correctRS
	// Check Header
	let lineError = true;
	for (let nn = 0; nn < correctRS[0].length; nn++) {
		if (userRS[0][nn] !== correctRS[0][nn]) {
			lineError = false;
		}
	}
	if (lineError === true) {
		userRS[0][userRS[0].length - 1] = true;
	} else {
		userRS[0][userRS[0].length - 1] = '';
	}

	// Check body
	for (let n = 1; n < correctRS.length; n++) {
		let findesamething = false;
		for (let i = 1; i < userRS.length; i++) {
			if (userRS[i][0] === correctRS[n][0]
					&& userRS[i][userRS[i].length - 1] !== true
					&& findesamething === false) {
				let line = true;
				for (let ii = 1; ii < correctRS[n].length; ii++) {
					if (!(userRS[i][ii] === correctRS[n][ii])) {
						line = false;
					}
				}
				if (line) {
					userRS[i][userRS[i].length - 1] = true;
					findesamething = true;
				}
			}
		}
	}

	// 5. Result
	let result = true;
	let error = '';
	for (let i = 0; i < userRS.length; i++) {
		if (userRS[i][userRS[i].length - 1] === '') {
			let a = i + 1;
			error += a + ' ';
			console.log(a);
			result = false;
		}
	}
	console.log('Result: ' + result + ' Fehler: ' + error);
	if (result === true) {
		$('#positivResult').append('Richtig!');
	} else {
		$('#negativResult').append('Fehler in Zeile: ' + error);
	}
}

function fillInSolution(RS) {
	for (let i = 0; i < RS.length; i++) {
		for (let ii = 0; ii < RS[i].length; ii++) {
			$('input:text[id=RS' + i + ii + ']').val(RS[i][ii]);
		}
	}
}

function resetTable(correctRS) {
	for (let n = 0; n < correctRS.length; n++) {
		for (let nn = 0; nn < correctRS[n].length + 1; nn++) {
			$('input:text[id=RS' + n + nn + ']').val('');
		}
	}
}

function accident(min, max) {
	/*
	 * hier wird eine Zufallszahl x erstellt
	 *  min <= x < max
	 * Daher muss der Maximalwert immer so aussehen: 'Anzahl der Aussagemöglichkeiten' + 1 = max
	 */
	return Math.floor(Math.random() * (max - min)) + min;
}
