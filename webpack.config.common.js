const glob = require('glob');
const path = require('path');
const webpack = require('webpack');

const HTMLWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const generateBefehlePlugins = () => glob.sync('./src/**/*.handlebars').map(
    dir => new HTMLWebpackPlugin({
        filename: dir.replace('./src/', '').replace('handlebars', 'html'), // Output
        template: dir, // Input
        favicon: './src/favicon.ico'
    }),
);

module.exports = {
    entry: ['./src/js/app.js', './src/style/main.scss'],
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'app.bundle.js',
    },
    module: {
        rules: [{
                test: /\.js$/,
                loader: 'babel-loader',
            },
            {
                test: /\.html$/,
                include: [
                    path.resolve(__dirname, "Informatik/Programmierung/Befehle_Variablen_Parameter")
                ],
                use: [{
                    loader: 'raw-loader',
                    options: {
                        outputPath: 'Informatik/Programmierung/Befehle_Variablen_Parameter',
                    },
                }, ],
            },
            {
                test: /\.handlebars$/,
                loader: "handlebars-loader",
                options: {
                    partialDirs: [path.join(__dirname, './src/partials')],
                    helperDirs: [
                        path.join(__dirname, './src/helpers')
                    ],
                    precompileOptions: {
                        knownHelpersOnly: false,
                    },
                },
            },
            {
                test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        outputPath: 'fonts/',
                    },
                }],
            },
        ],
    },
    plugins: [
        new CleanWebpackPlugin(),
        new webpack.LoaderOptionsPlugin({
            options: {
                handlebarsLoader: {}
            }
        }),
        new HTMLWebpackPlugin({
            title: 'OVK Informatik TU Dresden',
            template: './src/index.handlebars',
            favicon: './src/favicon.ico'
        }),
        ...generateBefehlePlugins(),
    ],
    stats: {
        colors: true,
    },
    devtool: 'source-map',
};