const webpack = require('webpack');
const merge = require('webpack-merge').merge;
const common = require('./webpack.config.common.js');

module.exports = merge(common, {
    mode: 'development',
    devServer: {
        static: {
            directory: 'src/static',
            watch: true,
        },
        hot: true,
        open: true,
        port: process.env.PORT || 9000,
        host: process.env.HOST || 'localhost',
    },
    module: {
        rules: [{
                test: /\.(sass|scss)$/,
                use: [
                    // Inserts all imported styles into the html document
                    { loader: "style-loader" },

                    // Translates CSS into CommonJS
                    { loader: "css-loader" },

                    // Compiles Sass to CSS
                    {
                        loader: "sass-loader"
                    },
                    // Reads Sass vars from files or inlined in the options property
                    {
                        loader: "@epegzz/sass-vars-loader",
                        options: {
                            syntax: 'scss',
                            // Option 1) Specify vars here
                            vars: {
                                domainRoot: JSON.stringify('')
                            }
                        }
                    }
                ]
            },

        ],
    },
    plugins: [
        new webpack.DefinePlugin({
            __DOMAIN_ROOT__: JSON.stringify(''),
        })
    ],
});